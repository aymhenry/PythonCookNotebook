<center>
    
<h1>Python cook notebook</h1>
    
<b>Adrien Henry</b>
</center>


The purpose of [this notebook](https://nbviewer.jupyter.org/urls/gitlab.com/aymhenry/PythonCookNotebook/raw/master/PythonCookNotebook.ipynb) is to have a rapid overview on how python works and to have a first grasp of its functionalities.



## Installation

There are multiple ways of installing python but the simplest is to use a distribution that comes with the python interpreter plus a series of useful packages. 

I recommand the anaconda distribution that can be downloaded here:

[https://www.anaconda.com/download/](https://www.anaconda.com/download/)

The support of python 2.7 has stopped. It's better to download python 3.x.

## Runing Jupyter

Once anaconda is installed, jupyter is launched with the following command:

**bash**
```bash
$ jupyter notebook
```

Or from the *anaconda* application.


## Licence
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This notebook is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
